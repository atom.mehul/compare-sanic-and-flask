from sanic import Sanic
from sanic.response import text
from sanic.response import json
from pymongo import MongoClient


app = Sanic()


app.config['mongodb_URI'] = 'localhost'
app.config['mongodb_port'] = '27017'

@app.route('/resource', methods=['POST'])
def post_resource(request):
	user_detail = {} 
	req_data = request.json

	user_detail['username'] = req_data['username']
	user_detail['email'] = req_data['email']
	user_detail['firstname'] = req_data['firstname']
	user_detail['lastname'] = req_data['lastname']
	
	client = MongoClient(app.config['mongodb_URI'], int(app.config['mongodb_port']))
	db = client['compareSanicAndFlaskDB']
	users = db.users

	result = users.insert_one(user_detail)
	return text('One post: {0}'.format(result.inserted_id))



@app.route('/resource', methods=['GET'])
def get_resource(request):
	

	all_users = []

	client = MongoClient(app.config['mongodb_URI'], int(app.config['mongodb_port']))
	db = client['compareSanicAndFlaskDB']
	users = db.users

	users = users.find({})
	for user in users :
		all_users.append(user)


	return (text(all_users))




if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)


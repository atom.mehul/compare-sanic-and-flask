from locust import HttpLocust, TaskSet, task
import json


payload = { "username": "mirella123",
			"email": "mirella.cardoso@inen.com",
			"firstname" : "Mirella",
			"lastname" : "Cardoso"
          }


headers = {'content-type': 'application/json'}




class UserBehavior(TaskSet):

	@task
	def get_workflow(self):
		response = self.client.get("/resource")
		print("Get status is :", response.status_code)
		#print("Response content:", response.text)
	
	@task
	def post_workflow(self):

		print(json.dumps(payload))

		response = self.client.post("/resource",data=json.dumps(payload),headers=headers)
		print("Post status is :", response.status_code)
    #print("Response content:", response.text)






class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    host= 'http://54.191.81.32:8082'
    min_wait = 0
    max_wait = 0

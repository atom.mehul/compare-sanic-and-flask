from flask import Flask
from flask import request
from pymongo import MongoClient


app = Flask(__name__)


app.config['mongodb_URI'] = 'localhost'
app.config['mongodb_port'] = '27017'






@app.route('/resource', methods=['POST'])
def post_resource():
	user_detail = {} 
	req_data = request.get_json()

	user_detail['username'] = req_data['username']
	user_detail['email'] = req_data['email']
	user_detail['firstname'] = req_data['firstname']
	user_detail['lastname'] = req_data['lastname']
	
	client = MongoClient(app.config['mongodb_URI'], int(app.config['mongodb_port']))
	db = client['compareSanicAndFlaskDB']
	users = db.users

	result = users.insert_one(user_detail)
	return 'One post: {0}'.format(result.inserted_id)



@app.route('/resource', methods=['GET'])
def get_resource():
	

	all_users = []

	client = MongoClient(app.config['mongodb_URI'], int(app.config['mongodb_port']))
	db = client['compareSanicAndFlaskDB']
	users = db.users

	users = users.find({})
	for user in users :
		all_users.append(user)


	return (str	(all_users))




if __name__ == '__main__':
	app.run(host='0.0.0.0',port=8082)




















	